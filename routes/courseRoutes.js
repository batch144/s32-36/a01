
const express = require("express");
const router = express.Router();

const courseControllers = require("./../controllers/courseControllers");


const auth = require("./../auth");


//create a course
router.post("/create-course", auth.verify, (req, res) => {
	courseControllers.createCourse(req.body).then(result => res.send(result))
})

//get all courses
router.get("/", (req, res) => {
	courseControllers.getAllcourses().then(result => res.send(result))
})

//retrieve only active courses
router.get("/active-courses", auth.verify, (req, res) => {
	courseControllers.getActivecourses().then(result => res.send(result))
})

//get a spcific course using findOne()
router.get("/specific-course", auth.verify, (req, res) => {

	//console.log(req.body)	//object
	courseControllers.getSpecificCourse(req.body.courseName).then( result => res.send(result))
})

//get specific course using findByID
router.get("/:courseId", auth.verify, (req, res) => {

	//console.log(req.params)	//object
	let paramsId = req.params.courseId
	courseControllers.getCourseById(paramsId).then( result => res.send(result))
})

//update isActive status of the course using findOneAndUpdate
	//update isActive to false
router.put("/archive", auth.verify, (req, res) => {

	courseControllers.archiveCourse(req.body.courseName).then( result => res.send(result))
})

	////update isActive to true
router.put("/unarchive", auth.verify, (req, res) => {

	courseControllers.unarchiveCourse(req.body.courseName).then( result => res.send(result))
})

//------------
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseControllers.archiveCourseById(req.params.courseId).then( result => res.send(result))
})


router.put("/:courseId/unarchive", auth.verify, (req, res) => {

	courseControllers.unarchiveCourseById(req.params.courseId).then( result => res.send(result))
})

//delete course using findOneAndDelete
router.delete("/delete-course", auth.verify, (req, res) => {

	courseControllers.deleteCourse(req.body.courseName).then( result => res.send(result))
})

//delete course using findByIdAndDelete()
router.delete("/:courseId/delete-course", auth.verify, (req, res) => {

	courseControllers.deleteCourseById(req.params.courseId).then( result => res.send(result))
})

//update course details
router.put("/:courseId/edit", auth.verify, (req, res) => {

	courseControllers.editCourse(req.params.courseId, req.body).then( result => res.send(result))
})



module.exports = router;