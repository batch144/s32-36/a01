
const express = require("express");
const router = express.Router();

const userControllers = require("./../controllers/userControllers");
const auth = require("./../auth")


//check if email exists
router.post("/email-exists", (req, res) => {

	userControllers.checkEmail(req.body).then(result => res.send(result))
})

//register a user
// http://localhost:4000/api/users
router.post("/register", (req, res) => {

	//console.log(req.body)
	userControllers.register(req.body).then(result => res.send(result))

})

//retrieve all users
router.get("/", (req, res) => {

	userControllers.getAllUsers().then(result => res.send(result))

})

//login
router.post("/login", (req, res) => {

	userControllers.login(req.body).then(result => res.send(result))
})

//retrieve user information
router.get("/details", auth.verify, (req, res) => {
	//id
	//email
	//isAdmin
	let userData = auth.decode(req.headers.authorization)


	userControllers.getProfile(userData).then(result => res.send(result))
})

//enroll user
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userControllers.enroll(data).then( result => res.send(result))
})



module.exports = router;