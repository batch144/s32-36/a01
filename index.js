
//import express module
const express = require("express");
const mongoose = require("mongoose");

// express server
const app = express();
//local port and available port
const PORT = process.env.PORT || 4000;
const cors = require("cors");

//import routes module
const courseRoutes = require("./routes/courseRoutes");
const userRoutes = require("./routes/userRoutes");


//mongodb connection
mongoose.connect('mongodb+srv://admin:admin@batch139.idzre.mongodb.net/course-booking?retryWrites=true&w=majority',
	{useNewUrlParser: true, useUnifiedTopology: true});


//mongodb notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


//middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors());


//routes
app.use("/api/courses", courseRoutes);
app.use("/api/users", userRoutes);



app.listen(PORT, () => console.log(`Server running at port ${PORT}`));